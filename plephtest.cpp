#include "jpleph.h"
#include "stdio.h"

#define J2000 2451545.

void printLoc(double loc[6])
{
  int i;
  for (i = 0; i < 2; i++) {
    printf("%f, %f, %f\n", loc[2*i], loc[2*i+1], loc[2*i+2]);
  }
}

void printArr(double a[365])
{
  int i;
  printf("[");
  for (i = 0; i < 365; i++) {
    printf("%f, ", a[i]);
  }
  printf("]\n");
}

void printArr(FILE* fh, double a[365])
{
  int i;
  for (i = 0; i < 365; i++) {
    fprintf(fh, "%f\n", a[i]);
  }
}

int main() {
  // compare earth, earth-moon barycenter
  void *ephem = jpl_init_ephemeris( "de405.dat", NULL, NULL);

  // julian date - but is it UTC or TT? probably TT...
  // this is oct 5, 2012
  float jd = 2456205.5;
  int rv;

  int i;
  for (i = 0; i < 5; i ++)  {
    double increment = (365./4.);
    double curjd = jd + increment*i;
    printf("date is %f\n", curjd);
    // heliocentric location of earth, should be cartesian (right?).
    double earth_loc_heliocentric[6];
    rv = jpl_pleph(ephem, curjd, 3, 11, earth_loc_heliocentric, 1);
    // heliocentric location of earth-moon barycenter
    double emb_loc_heliocentric[6];
    rv = jpl_pleph(ephem, curjd, 13, 11, emb_loc_heliocentric, 1);
    if (rv != 0) {
      fprintf(stderr, "Uh oh! error %d\n", rv);
    }
    printf("Location/vel of earth:\n");
    printLoc(earth_loc_heliocentric);
    printf("Location/vel of earth-moon barycenter:\n");
    printLoc(emb_loc_heliocentric);
    printf("\n");
  }
  jd = 2456205.5;
  double xs[365], ys[365], zs[365];
  for (i = 0; i < 365; i++) {
    double curjd = jd + i;
    printf("curJd = %f\n", curjd);
    // heliocentric location of earth, should be cartesian (right?).
    double earth_loc_heliocentric[6];
    rv = jpl_pleph(ephem, curjd, 3, 11, earth_loc_heliocentric, 1);
    if (rv != 0) {
      fprintf(stderr, "Uh oh! error %d\n", rv);
    }
    xs[i] = earth_loc_heliocentric[0];
    ys[i] = earth_loc_heliocentric[1];
    zs[i] = earth_loc_heliocentric[2];    
  }

  FILE* fh = fopen("xs.txt", "w");
  printArr(fh, xs);
  fh = fopen("ys.txt", "w");
  printArr(fh, ys);
  fh = fopen("zs.txt", "w");
  printArr(fh, zs);
  return 0;
}
