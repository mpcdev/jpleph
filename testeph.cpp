/* testeph.cpp: verify a JPL ephemeris

Copyright (C) 2011, Project Pluto

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.    */

/*****************************************************************************
*        *****    jpl planetary and lunar ephemerides    *****     C ver.1.2 *
******************************************************************************
* program testeph                                                            *
*                                                                            *
*                                                                            *
* Testeph tests the jpl ephemeris reading and interpolating routine using    *
* examples computed from the original ephemeris.                             *
*                                                                            *
* Testeph contains the reading and interpolating subroutines that are of     *
* eventual interest to the user.  Once testeph is working correctly, the     *
* user can extract those subroutines and the installation process is         *
* complete.                                                                  *
*                                                                            *
* You must allow acces to "testpo.xxx" to testeph.                           *
* "testpo.xxx" is the specially formatted text file that contains the test   *
* cases for the ephmeris, dexxx.                                             *
*                                                                            *
* After the initial identifying text which is concluded by an "EOT" in       *
* columns 1-3, the test file contains the following quantities:              *
*                                                                            *
*     JPL ephemeris number                                                   *
*     calendar date                                                          *
*     julian ephemeris date                                                  *
*     target number (1-mercury, ...,3-earth, ,,,9-pluto, 10-moon, 11-sun,    *
*                    12-solar system barycenter, 13-earth-moon barycenter    *
*                    14-nutations, 15-librations)                            *
*     center number (same codes as target number)                            *
*     coordinate number (1-x, 2-y, ... 6-zdot)                               *
*     coordinate  [au, au/day].                                              *
*                                                                            *
* For each test case input, testeph                                          *
*                                                                            *
*     - computes the corresponding state from data contained                 *
*       in dexxx,                                                            *
*                                                                            *
*     - compares the two sets,                                               *
*                                                                            *
*     - writes an error message if the difference between                    *
*       any of the state components is greater than 10**(-13).               *
*                                                                            *
*     - writes state and difference information for every npt'th             *
*       test case processed.                                                 *
*                                                                            *
*                                                                            *
*  This program was written in standard fortran-77 and it was manually       *
*  translated to C language by Piotr A. Dybczynski (dybol@phys.amu.edu.pl).  *
*                                                                            *
*  This is version 1.2 of this C translation, use jplbin.h version 1.2       *
*
******************************************************************************
*                 Last modified: July 23, 1997 by PAD                        *
******************************************************************************
16 Mar 2001:  Revised by Bill J. Gray.  You can now use binary
ephemerides with either byte order ('big-endian' or 'small-endian');
the code checks to see if the data is in the "wrong" order for the
current platform,  and swaps bytes on-the-fly if needed.  (Yes,  this
can result in a slowdown... sometimes as much as 1%.  The function is
so mathematically intensive that the byte-swapping is the least of our
troubles.)  You can also use DE-200, 403, 404, 405,  or 406 and most,
if not all,  later ephemerides without recompiling (the constan( )
function now determines which ephemeris is in use and its byte order);
and you can set the TESTFILE and EPHFILE on the command line.

Also,  I did some minor optimization of the interp( ) (Chebyshev
interpolation) function,  resulting in a bit of a speedup.
*****************************************************************************/

#include<stdio.h>
#include<math.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>

/**** include variable and type definitions, specyfic for this C version */

#include"jpleph.h"

void error_exit( const int err_code)
{
   printf( "'testeph' requires the name of the JPL ephemeris file as a command\n");
   printf( "line argument.  It then looks for a 'testpo' (test positions) file\n");
   printf( "in the same folder with the same extension,  and checks the positions\n");
   printf( "against those computed from the ephemeris.\n");
   exit( err_code);
}

/***** THERE IS NO NEED TO MODIFY THE REST OF THIS SOURCE (I hope) *********/

int main( const int argc, const char **argv)
{
  char nams[400][6], buff[102];
  double del, et, r[6], vals[400], xi;
  int i, line, ntarg, nctr, ncoord, n_failures = 0;
  int n_constants, n_columns;
  int output_frequency = 100;
  const char *ephfile_name = argv[1];
  double start_jd, end_jd;
  FILE *testfile;
  clock_t timer;
  void *ephem;
#if defined( __GNUC__) && !defined( __MINGW32__)
   const char path_separator = '/';
#else
   const char path_separator = '\\';
#endif

/***** Write a fingerprint to the screen. ***********************************/

  setvbuf( stdout, NULL, _IONBF, 0);
  puts("\n JPL test-ephemeris program (v.1.2)\n"
       " C version translated from the original JPL FORTRAN code.\n");

   if( argc < 2)
      error_exit( -1);

   for( i = 2; i < argc; i++)
      if( argv[i][0] == '-')
         switch( argv[i][1])
            {
            case 'f': case 'F':
               output_frequency = atoi( argv[i] + 2);
               break;
            default:
               printf( "Unrecognized option '%s'\n", argv[i]);
               error_exit( -2);
               break;
            }

/****** Print the ephemeris constants. **************************************/

  ephem = jpl_init_ephemeris( ephfile_name, nams, vals);
  if( !ephem)
     {
     printf( "Ephemeris file '%s' not loaded\n", ephfile_name);
     error_exit( -2);
     }
   else
      printf( "Ephemeris initialized\n");

   n_constants = (int)jpl_get_long( ephem, JPL_EPHEM_N_CONSTANTS);
   printf( "%d constants\n", n_constants);
   start_jd = jpl_get_double( ephem, JPL_EPHEM_START_JD),
   end_jd =   jpl_get_double( ephem, JPL_EPHEM_END_JD),
   printf("%.9f  %.9f  %.9f\n", start_jd, end_jd,
                           jpl_get_double( ephem, JPL_EPHEM_STEP));
   n_columns = (n_constants + 1) / 2;
   for( i = 0; i < n_columns; i++)
      {
      printf("%.6s  %24.16E",nams[i],vals[i]);
      if( i + n_columns < n_constants)
         printf("   %.6s  %24.16E",nams[i + n_columns],vals[i + n_columns]);
      printf( "\n");
      }

   printf( "emrat = %.15lf      AU = %.5lf\n",
                           jpl_get_double( ephem, JPL_EPHEM_EARTH_MOON_RATIO),
                           jpl_get_double( ephem, JPL_EPHEM_AU_IN_KM));

/****** Skip the test points file header comments.  *************************/
  strcpy( buff, ephfile_name);
  for( i = strlen( buff); i && buff[i - 1] != path_separator; i--)
     ;
   sprintf( buff + i, "testpo.%3ld",
            jpl_get_long( ephem, JPL_EPHEM_EPHEMERIS_VERSION));

  testfile = fopen( buff, "r");
  if( !testfile)
     {
     printf( "Test data file '%s' not found\n", buff);
     error_exit( -3);
     }

   while( fgets( buff, 100, testfile) && memcmp( buff, "EOT", 3))
      ;

   puts(" LINE  JED    t# c# x#  --- JPL value ---   "
          "--- user value --   -- difference --");

   line=0;
   timer = clock( );
   while( fgets(buff,100,testfile) != NULL)
   {
     int err_code;
/*****  Read a value from the test case; Skip if not within the time-range
        of the present version of the ephemeris.                            */
     sscanf(buff+15," %lf %d %d %d %lf",&et,&ntarg,&nctr,&ncoord,&xi);

     err_code = jpl_pleph(ephem, et, ntarg, nctr, r, 1);
     del  = r[ncoord-1] - xi;
     if( err_code)
        {
        n_failures++;
        if( n_failures == 1)
           {
           const double J2000 = 2451545.;

           printf("%4d %10.1f %2d %2d %2d %17.13f %17.13f %17.13f\n",
               line,et,ntarg,nctr,ncoord,xi,r[ncoord-1],del);
           printf( "WARNING:  The test file tests items outside the range\n");
           printf( "of this ephemeris!\n");
           printf( "The input ephemeris file covers years from %.1lf to %.1lf.\n",
                  2000. + (start_jd - J2000) / 365.25,
                  2000. + (end_jd - J2000) / 365.25);
           printf( "The test is for the year %.1lf\n",
                  2000. + (et - J2000) / 365.25);
           printf( "  Hit any key.  Tests that _are_ within the valid range\n");
           printf( "of the ephemeris file will still be run.\n");
           getchar( );
           }
        }

     else
        {
        const double tolerance = (ntarg == 15 ? 1e-12 : 1e-13);
        const double del2 = fabs(r[ncoord-1] -xi)/ ( fabs(r[ncoord-1])+fabs(xi) );

        line++;

        if( !(line % output_frequency))
           printf("%4d %10.1f %2d %2d %2d %17.13f %17.13f %17.13f\n",
               line,et,ntarg,nctr,ncoord,xi,r[ncoord-1],del);

/******  Print out warning if difference greater than tolerance.   **********/

        if( fabs(del) >= tolerance && del2 > tolerance)
           {
             printf( "*****  warning : next difference >= 1e-%s *****\n",
                        (ntarg == 15 ? "12" : "13"));
             printf("%d %10.1f %2d %2d %2d %17.13f %17.13f %17.13f\n",
                 line,et,ntarg,nctr,ncoord,xi,r[ncoord-1],del);
             getchar();
           }
        }
     /*getchar();*/            /* uncoment this to stop after every warning */
   }

   printf( "%d lines read and tested in %.3lf seconds\n", line,
           (double)( clock( ) - timer) / (double)CLOCKS_PER_SEC);
   if( n_failures)
      {
      printf( "%d lines were outside the range of the ephemeris file and\n",
                  n_failures);
      printf( "were therefore not actually tested.\n");
      }
   fclose( testfile);
   jpl_close_ephemeris( ephem);
   return( 0);
}
