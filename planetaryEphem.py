#!/usr/bin/env python


""" jmyers 10/11/12 - a ctypes bridge between the JPL ephemeris reader
by Bill Gray and my Python code"""

import ctypes
import ctypes.util
import os.path

dir = os.path.dirname(os.path.realpath(__file__))
ephemFile = os.path.join(dir, "de405.dat")
#print "Loading JPL ephemeris from ", ephemFile
#load from SO file
jpleph_so = ctypes.cdll.LoadLibrary("libjpleph.so")
#load the function pointers for easy calling
pleph_fun = jpleph_so.pleph_for_python
#jpl_init = jpleph_so.jpl_init_ephemeris
#jpl_pleph = jpleph_so.jpl_pleph

SixDoublesArrayType = ctypes.c_double * 6

#jpl_pleph.argtypes = [ctypes.c_int32, ctypes.c_double, ctypes.c_int,
#                      ctypes.c_int, SixDoublesArrayType, ctypes.c_int]

NULL = ctypes.c_int(0)

##init the JPL ephemeris right away
#ephemPtr = jpl_init(os.path.join(dir, "de405.dat"),
#                    NULL, NULL)

# some constants
[MERCURY,VENUS,EARTH,MARS,JUPITER,SATURN,URANUS,
 NEPTUNE, PLUTO, MOON, SUN, SS_BARYCENTER,
 EARTH_MOON_BARYCENTER] = range(1,14)

ERRCODES={(-1):"JPL_EPH_OUTSIDE_RANGE",
          (-2):"JPL_EPH_READ_ERROR",            
          (-3):"JPL_EPH_NO_NUTATIONS_IN_EPHEMERIS",
          (-4):"JPL_EPH_NO_LIBRATIONS_IN_EPHEMERIS",
          (-5):"JPL_EPH_INVALID_INDEX",
          (-6):"JPL_EPH_FSEEK_ERROR"} 


def jplPlanetaryEphemeris(time, target, center):
    """ return the 3D cartesian x,y,z position of body target
    (e.g. Earth) relative to center (e.g. Sun) at the specified time
    (time is in units of julian days.  Timescale is 'ephemeris time'
    according to comments)."""
    #ephPtr = ctypes.c_int64(ephemPtr)
    et = ctypes.c_double(time)
    ntarg = ctypes.c_int(target)
    ncent = ctypes.c_int(center)
    retArray = SixDoublesArrayType()
    #calc_velocity = ctypes.c_int(0)
    #rv = jpl_pleph(ephemPtr, et, ntarg, ncent, retArray, calc_velocity)
    rv = pleph_fun(ephemFile, et, ntarg, ncent, retArray)
    if rv != 0:
        if rv in ERRCODES.keys():
            meaning = ERRCODES[rv]
            raise Exception("jpl_pleph(et=%f, ntarg=%d, ncent=%d) gave error %d: %s" \
                                % (time, target, center, rv, meaning))
        else:
            raise Exception("jpl_pleph returned non-zero code %d" % rv)

    [x,y,z, dx, dy, dz] = retArray
    return [x,y,z, dx, dy, dz]

if __name__ == "__main__":
    #print "ephemPtr is: ", ephemPtr
    [x,y,z, dx, dy, dz] = jplPlanetaryEphemeris(2456205.5, EARTH, SUN)
    print x, y, z
    print dx, dy, dz
    [x,y,z, dx, dy, dz] = jplPlanetaryEphemeris(2456205.5+1, EARTH, SUN)
    print x, y, z
    print dx, dy, dz
